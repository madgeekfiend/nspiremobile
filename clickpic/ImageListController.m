//
//  ImageListController.m
//  clickpic
//
//  Created by Sam Contapay on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ImageListController.h"
#import "UploadController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "DSActivityView.h"
#import "Constants.h"
#import "UIImage+Resize.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "NSpireManager.h"
#import "AppDelegate.h"
#import "JSONKit.h"
#import "ImageCell.h"
#import "UIImageView+AFNetworking.h"
#import "SHK.h"
#import "PictureDetailController.h"
#import "ShareDialogController.h"

@interface ImageListController()
-(void)toolbarClicked:(id)sender;
@end


@implementation ImageListController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.navigationController setToolbarHidden:NO];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *action = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(toolbarClicked:)];
    self.toolbarItems = [NSArray arrayWithObjects:flex,action,flex, nil];
    
    bannerView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    bannerView.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait, ADBannerContentSizeIdentifierLandscape, nil];
    bannerView.delegate = self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:NO];

}

- (void)viewDidAppear:(BOOL)animated
{
    if ( pickedImage != nil )
    {
        [self editPicture];
    }
    
    if ( [[NSpireManager sharedApplication] getToken].length < 1 )
    {
        // Shove use to login because they have to login first
        self.tabBarController.selectedIndex = 1;
        UIAlertView *login = [[UIAlertView alloc] initWithTitle:@"Login" message:@"You must login or create a new account first" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [login show];
    }
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
                               
#pragma mark - Toolbar item picked
-(void)toolbarClicked:(id)sender
{
    NSLog(@"TOOLBAR ITEM CLICKED"); 
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Get picture from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Library", nil];
    //[as showInView:self.view];
    [as showFromTabBar:self.tabBarController.tabBar];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[NSpireManager sharedApplication] getAllImages].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"image";
    NSArray *images = [[NSpireManager sharedApplication] getAllImages];
    
    ImageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    NSManagedObject *image = [images objectAtIndex:indexPath.row];
    
    NSDate *dt = [image valueForKey:@"created_at"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:dt];
    NSLog(@"date: %@", dateString);
    
    cell.slug.text = [NSString stringWithFormat:@"Uploaded: %@", dateString];
//    NSString *urlstring = [NSString stringWithFormat:@"%@/%@", BASE_URL, [image valueForKey:@"url"]];
//    NSURL *url = [NSURL URLWithString:urlstring];
    
    NSURL *url = [NSURL URLWithString: [image valueForKey:@"url"]];
    [cell.image setImageWithURL:url];
    
    return cell;
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( 2 == buttonIndex ) return;
    
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = (id)self;
    if ( 0 == buttonIndex ) // Use the camera
    {
        if ( ![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
        {
            UIAlertView *noCamera = [[UIAlertView alloc] initWithTitle:@"No Camera Found" message:@"Your device does not have a camera. Please select 'Photo Library'." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [noCamera show];
            return;
        }
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ( 1 == buttonIndex )
    {
        if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] )
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    imgPicker.allowsEditing = NO;
    imgPicker.mediaTypes = [NSArray arrayWithObject:(NSString*)kUTTypeImage];
    [self presentModalViewController:imgPicker animated:YES];                        
}

#pragma mark - UIImagePicker delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissModalViewControllerAnimated:YES];
    
    UIImage *picked = [info objectForKey:UIImagePickerControllerOriginalImage];
    pickedImage = picked;
    if ( picker.sourceType == UIImagePickerControllerSourceTypeCamera ) // Save image
        UIImageWriteToSavedPhotosAlbum(picked, nil, nil, nil);
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - AFPhotoEditor controller
- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image
{
    pickedImage = nil;
     
    //[self sendPicture:image];
    [editor dismissModalViewControllerAnimated:YES];
    
    // Let's bring up the other dialog
    ShareDialogController *dlg = (ShareDialogController*)[QuickDialogController controllerForRoot:[ShareDialogController createShareDialog]];
    dlg.img = image;
    dlg.delegate = self;
    [SHK setRootViewController:self];
    [self.navigationController setToolbarHidden:YES];
    [self.navigationController pushViewController:dlg animated:YES];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( editingStyle == UITableViewCellEditingStyleDelete ) // Delete this
    {
        // Delete the file
        NSManagedObject *image = [[[NSpireManager sharedApplication] getAllImages] objectAtIndex:indexPath.row];
        NSString *slug = [image valueForKey:@"slug"];
        NSString *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.jpg", slug]];
        
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:jpgPath error:nil];
        
        [[NSpireManager sharedApplication] deleteAtIndex:indexPath.row];
        [self.tableView reloadData];
    }
}

- (void)photoEditorCanceled:(AFPhotoEditorController *)editor
{
    // Handle cancelation here
    pickedImage = nil;
    [editor dismissModalViewControllerAnimated:YES];
}

#pragma mark - Picture Functions
-(void)editPicture
{
    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc]initWithImage:pickedImage];
    [editorController setDelegate:self];
    [self presentModalViewController:editorController animated:YES]; 
}

-(void)onFinishUploadingImage
{
    [self.tableView reloadData];
}

#pragma mark - Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *ip = [self.tableView indexPathForSelectedRow];
    NSManagedObject *obj = [[[NSpireManager sharedApplication] getAllImages] objectAtIndex:ip.row];
    NSString *slug = [obj valueForKey:@"slug"];
    
    /*
    NSError *error;
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    // Write out the contents of home directory to console
    NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
    */
    
    // It's only going to be one segue
    PictureDetailController *vc = [segue destinationViewController];
    // Now set the image in the view controller
    NSString *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.jpg", slug]];
    NSLog(@"Retrieve path: %@", jpgPath);
    vc.file_path = jpgPath;
    vc.slug = slug;
}

#pragma mark - Ad delegate

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    //self.tableView.tableHeaderView = bannerView;
}


@end
