//
//  Utils.h
//  clickpic
//
//  Created by Sam Contapay on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(Utils*)sharedInstance;

// Utilities
- (BOOL) validateEmail: (NSString *) candidate;


@end
