//
//  ShareDialogController.h
//  clickpic
//
//  Created by Sam Contapay on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@protocol ShareDialogDelegate <NSObject>

@required
    -(void)onFinishUploadingImage;

@end

@interface ShareDialogController : QuickDialogController
{
    UIImage* img;
    NSString *image_slug;
}

@property(nonatomic,strong)UIImage* img;
@property(nonatomic,strong)NSString *slug;
@property(nonatomic,weak)id<ShareDialogDelegate> delegate;

+(QRootElement*)createShareDialog;
-(void)sendPicture:(UIImage*)pic withParms:(NSMutableDictionary*)options useFacebook:(BOOL)fb useTwitter:(BOOL)twit useEmail:(BOOL)email;

+(QRootElement*)createLinkShareDialog;

@end
