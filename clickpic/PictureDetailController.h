//
//  PictureDetailController.h
//  clickpic
//
//  Created by Sam Contapay on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureDetailController : UIViewController
{
    NSString *filePath;
    NSString *slug;
}

@property(nonatomic,retain)NSString *file_path;
@property(nonatomic,retain)NSString *slug;

@property (weak, nonatomic) IBOutlet UIImageView *img;
- (IBAction)shareImage:(id)sender;
@end
