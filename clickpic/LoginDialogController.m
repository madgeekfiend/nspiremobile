//
//  LoginDialogController.m
//  clickpic
//
//  Created by Sam Contapay on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginDialogController.h"
#import "Utils.h"
#import "Constants.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "JSONKit.h"
#import "NSpireManager.h"

@interface LoginDialogController ()
    -(void)onCreateUser;
@end

@implementation LoginDialogController

-(void)viewDidLoad
{
    self.root = [[QRootElement alloc] init];
    self.root.grouped = YES;
    self.root.controllerName = @"LoginDialogController";
    
    QSection *login = [[QSection alloc] initWithTitle:@"Login"];
    QEntryElement *username = [[QEntryElement alloc] initWithTitle:@"Email" Value:[[NSpireManager sharedApplication] getUsername] Placeholder:@"Enter a valid email"];
    username.key = @"username";
    username.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [login addElement:username];
    QEntryElement *password = [[QEntryElement alloc] initWithTitle:@"Password" Value:[[NSpireManager sharedApplication] getPassword] Placeholder:@"Enter your password"];
    password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    password.secureTextEntry = YES;
    password.key=@"password";
    [login addElement:password];
    
    [self.root addSection:login];
    
    QSection *log = [[QSection alloc] init];
    log.key = @"logsection";
    QButtonElement *LoginButton = [[QButtonElement alloc] initWithTitle:@"Login/Create"];
    LoginButton.controllerAction = @"onCreateUser";
    if ( [[NSpireManager sharedApplication] isUserLoggedIn] )
    {
        NSString *footer = [NSString stringWithFormat:@"You are currently logged in. View your profile at http://www.nsprme.com/profile/%@.",[[NSpireManager sharedApplication] getNickname]];
        log.footer = footer;
    }
    else
        log.footer = @"You are not logged in. If you wish to login via Facebook @ nsprme.com please register using your facebook account's main email address. For security, we recommend you do not use the same password you use to login to Facebook.";
    
    
    [log addElement:LoginButton];

    [self.root addSection:log];

}

#pragma mark - button actions

-(void)onCreateUser
{
    NSLog(@"Create me");
    // Get value for email text
    QRootElement *root = self.root;
    NSMutableDictionary *parms = [[NSMutableDictionary alloc] initWithCapacity:10];
    [root fetchValueIntoObject:parms];
    
    if ( ![[Utils sharedInstance] validateEmail:[parms objectForKey:@"username"]] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Invalid Email" message:@"You have entered an invalid email. Please try again." delegate:nil cancelButtonTitle:@"Re-Enter email" otherButtonTitles: nil];
        [av show];
        return;
    }
    
    if ( [[parms objectForKey:@"password"] length] < 6 )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Invalid Password" message:@"You have entered an invalid password. Minimum of 6 chacters required. Please try again." delegate:nil cancelButtonTitle:@"Re-Enter Password" otherButtonTitles: nil];
        [av show];
        return; 
    }
    
    NSString* sig = [[NSpireManager sharedApplication] signParams:parms];
    [parms setValue:sig forKey:@"sig"];
    // Everything is well let's go ahead and add the user to the stuff
    // the return object will be a JSON object that looks like this
    // { status: (200 we are good, 409 user exists, 422 entered ok but no room in beta), token: usertoken, message: message received }
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:url];
    [client postPath:@"/mobile/register" parameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ( operation.response.statusCode == 200 )
        {
            // We created a user YES BITCH show some shit
            JSONDecoder *decoder = [[JSONDecoder alloc] initWithParseOptions:JKParseOptionNone];
            NSDictionary *json = [decoder objectWithData:responseObject];
            NSLog(@"token: %@", [json objectForKey:@"token"]);
            
            // Now let's go ahead and persist this information
            [[NSpireManager sharedApplication] setUserDefaultUsername:[parms objectForKey:@"username"] withPassword:[parms objectForKey:@"password"] withToken:[json objectForKey:@"token"] withNickname:[json objectForKey:@"slug"] ];
            
            // Change to logged in
            QSection *log = [self.root getSectionForIndex: 1];
            NSString *footer = [NSString stringWithFormat:@"You are currently logged in. View your profile at http://www.nsprme.com/profile/%@.",[[NSpireManager sharedApplication] getNickname]];
            log.footer = footer;
            [self.tableView reloadData];

            UIAlertView *av = [[UIAlertView alloc] initWithTitle:[json objectForKey:@"title"] message:[json objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
        }
        else if ( operation.response.statusCode == 203 ) // Bad request so kill it
        {
            JSONDecoder *decoder = [[JSONDecoder alloc] initWithParseOptions:JKParseOptionNone];
            NSDictionary *json = [decoder objectWithData:responseObject];
            
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:[json objectForKey:@"title"] message:[json objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
        }
                    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Problem" message:@"There was a problem please try again or wait until later." delegate:nil cancelButtonTitle:@"Back" otherButtonTitles: nil];
        [av show];
    }];

}

@end
