// MD5 monkey patching implementation for NSString
//
// Gotten from http://iphonedevelopertips.com/core-services/create-md5-hash-from-nsstring-nsdata-or-file.html

@interface NSString(MD5) 

-(NSString*)MD5;

@end
