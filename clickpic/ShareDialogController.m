//
//  ShareDialogController.m
//  clickpic
//
//  Created by Sam Contapay on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ShareDialogController.h"
#import "UIImage+Resize.h"
#import "DSActivityView.h"
#import "NSpireManager.h"
#import "Constants.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "SHKMail.h"
#import "SHK.h"
#import "SHKTwitter.h"
#import "SHKFacebook.h"

@interface ShareDialogController()
    -(void)clickSend;
    -(void)clickReshare;
    -(void)shareUrl:(NSString*)url useEmail:(BOOL)email useTwitter:(BOOL)twitter useFacebook:(BOOL)facebook;
@end

@implementation ShareDialogController
@synthesize img, delegate;
@synthesize slug = image_slug;

-(void)clickSend
{
    NSLog(@"Sending Image");
    QRootElement *root = self.root;
    NSMutableDictionary *options = [[NSMutableDictionary alloc] initWithCapacity:10];
    [root fetchValueIntoObject:options];
    QBooleanElement *ele = (QBooleanElement*)[root elementWithKey:@"facebook"];
    BOOL useFacebook = ele.boolValue; // (BOOL)[options valueForKey:@"facebook"];
    ele = (QBooleanElement*)[root elementWithKey:@"email"];
    BOOL useEmail = ele.boolValue;
    ele = (QBooleanElement*)[root elementWithKey:@"twitter"];
    BOOL useTwitter = ele.boolValue;
    
    // Now that I got everything let's go ahead and pack it up and send
    [self sendPicture:self.img withParms:options useFacebook:useFacebook useTwitter:useTwitter useEmail:useEmail];

    [self.navigationController setToolbarHidden:NO];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)clickReshare
{
    QRootElement *root = self.root;
    QBooleanElement *ele = (QBooleanElement*)[root elementWithKey:@"facebook"];
    BOOL useFacebook = ele.boolValue; // (BOOL)[options valueForKey:@"facebook"];
    ele = (QBooleanElement*)[root elementWithKey:@"email"];
    BOOL useEmail = ele.boolValue;
    ele = (QBooleanElement*)[root elementWithKey:@"twitter"];
    BOOL useTwitter = ele.boolValue;
    
    // Create the share URL
    [self shareUrl:image_slug useEmail:useEmail useTwitter:useTwitter useFacebook:useFacebook];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)shareUrl:(NSString *)slug useEmail:(BOOL)email useTwitter:(BOOL)twitter useFacebook:(BOOL)facebook
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", SHORT_IMG_URL, slug];
    if ( email == YES )
    {
        NSURL *shareURL = [NSURL URLWithString:url];
        SHKItem *item = [SHKItem URL:shareURL title:@"[NSPRME.COM]I find this image inspiring!"];
        item.text = @"I found this picture inspiring and shared it on Nsprme.com! Please take a look and let me know what you think.";
        
        [SHKMail shareItem:item];
    }
    if ( twitter == YES )
    {
        NSURL *shareURL = [NSURL URLWithString:url];
        SHKItem *item = [SHKItem URL:shareURL title:@"Inspiring pic! #nsprme"];
        item.shareType = SHKShareTypeURL;
        
        [SHKTwitter shareItem:item];
    }
    if ( facebook == YES )
    {
        NSURL *shareURL = [NSURL URLWithString:url];
        SHKItem *item = [SHKItem URL:shareURL title:@"Photo"];
        item.shareType = SHKShareTypeURL;
        
        [SHKFacebook shareItem:item];
    }  
}

#pragma mark - Form Creation

+(QRootElement *)createShareDialog
{
    QRootElement *root = [[QRootElement alloc] init];
    root.controllerName = @"ShareDialogController";
    root.grouped = YES;
    root.title = @"Share";
    
    QSection *top = [[QSection alloc] init];
    QEntryElement *caption = [[QEntryElement alloc] initWithTitle:@"Caption" Value:@"" Placeholder:@"Enter caption"];
    caption.key = @"caption";
    [top addElement:caption];
    QEntryElement *tags = [[QEntryElement alloc] initWithTitle:@"Tags" Value:@"" Placeholder:@"Enter comma seperated tags"];
    tags.key = @"tags";
    tags.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [top addElement:tags];
    
    [root addSection:top];
    
    QSection *shareService = [[QSection alloc] initWithTitle:@"Share With"];
    QBooleanElement *facebook = [[QBooleanElement alloc] initWithTitle:@"Facebook" BoolValue:NO];
    facebook.key = @"facebook";
    QBooleanElement *email = [[QBooleanElement alloc] initWithTitle:@"Email" BoolValue:NO];
    email.key = @"email";
    QBooleanElement *twitter = [[QBooleanElement alloc] initWithTitle:@"Twitter" BoolValue:NO];
    twitter.key = @"twitter";
    [shareService addElement:facebook];
    [shareService addElement:email];
    [shareService addElement:twitter];
    
    [root addSection:shareService];
    
    QSection *btnSection = [[QSection alloc] init];
    QButtonElement *send = [[QButtonElement alloc] initWithTitle:@"Send"];
    send.controllerAction = @"clickSend";
    [btnSection addElement:send];
    
    [root addSection:btnSection];
    
    return root;
}

+(QRootElement*)createLinkShareDialog
{
    QRootElement *root = [[QRootElement alloc] init];
    root.controllerName = @"ShareDialogController";
    root.grouped = YES;
    root.title = @"Share";
    
    QSection *main = [[QSection alloc] initWithTitle:@"Share"];
    QBooleanElement *facebook = [[QBooleanElement alloc] initWithTitle:@"Facebook" BoolValue:NO];
    facebook.key = @"facebook";
    QBooleanElement *email = [[QBooleanElement alloc] initWithTitle:@"Email" BoolValue:NO];
    email.key = @"email";
    QBooleanElement *twitter = [[QBooleanElement alloc] initWithTitle:@"Twitter" BoolValue:NO];
    twitter.key = @"twitter";
    [main addElement:facebook];
    [main addElement:email];
    [main addElement:twitter];
    
    QSection *send = [[QSection alloc] init];
    QButtonElement *buttonSend = [[QButtonElement alloc] initWithTitle:@"Send"];
    buttonSend.controllerAction = @"clickReshare";
    [send addElement:buttonSend];
    
    [root addSection:main];
    [root addSection:send];
    
    return root;    
}

#pragma mark - Send Image

// We need to hand pick the parameters and only send caption and tags with the original params
-(void)sendPicture:(UIImage*)pic withParms:(NSMutableDictionary*)options useFacebook:(BOOL)fb useTwitter:(BOOL)twit useEmail:(BOOL)email
{
    //    NSString *url = [info objectForKey: UIImagePickerControllerMediaURL];
    //    NSLog(@"MEDIA URL: %@", url);
    
    // Setup activity monitor
    UIImage *resized = nil;
    if ( img.size.width > img.size.height )
       resized = [pic resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(640, 480) interpolationQuality:kCGInterpolationDefault];
    else
       resized = [pic resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(480, 640) interpolationQuality:kCGInterpolationDefault];
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:@"Uploading..."];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:BASE_URL] ];
    NSData *imgData = UIImageJPEGRepresentation(resized, 0.60);
    
    NSString *token = [[NSpireManager sharedApplication] getToken];
    if ( token.length < 1 )
    {
        UIAlertView *notloggedin = [[UIAlertView alloc] initWithTitle:@"Not Logged In" message:@"You are not logged in please login by going to 'Account'." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [notloggedin show]; return;
    }
    
    // Calculate sig
    NSString *sigthis = [NSString stringWithFormat:@"%@%@imageUpload", token, SEED_KEY]; 
    
    NSMutableDictionary *parms = [NSMutableDictionary dictionaryWithObject: token forKey:@"token"];
    [parms setValue:[[NSpireManager sharedApplication] signString:sigthis] forKey:@"sig"];
    [parms setValue:[options objectForKey:@"caption"] forKey:@"caption"];
    [parms setValue:[options objectForKey:@"tags"] forKey:@"tags"];
    NSLog(@"THE SIG %@", [[NSpireManager sharedApplication] signString:sigthis] );
    
    // Old path path:@"/images"
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"/mobile/image/upload" parameters:parms constructingBodyWithBlock:^(id<AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imgData name:@"file" fileName:@"image.jpg" mimeType:@"image/jpg"];
    }];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];    
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *JSON = [[NSpireManager sharedApplication] decodeJson:responseObject];
        NSLog(@"SLUG: %@, URL: %@", [JSON objectForKey:@"slug"], [JSON objectForKey:@"url"]);
        // Now add the managed object to it
        [[NSpireManager sharedApplication] saveImageWithURL:[JSON objectForKey:@"url"] withSlug:[JSON objectForKey:@"slug"] withDeleteHash:[JSON objectForKey:@"delete"]];
        [DSBezelActivityView removeView];
        
        // At this point I'm going to save the image
        NSString *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.jpg", [JSON objectForKey:@"slug"]]];
        NSLog(@"Save path: %@", jpgPath);
        [imgData writeToFile:jpgPath atomically:YES];
        
        if ( [delegate respondsToSelector:@selector(onFinishUploadingImage)] )
        {
            // Then call it
            [delegate onFinishUploadingImage];
        }
        
        // Now do whatever share
        [self shareUrl:[JSON objectForKey:@"slug"] useEmail:email useTwitter:twit useFacebook:fb];
        
        /*
        NSString *imgUrl = [NSString stringWithFormat:@"%@/%@", SHORT_IMG_URL, [JSON objectForKey:@"slug"]];
        if ( email == YES )
        {
            NSURL *shareURL = [NSURL URLWithString:imgUrl];
            SHKItem *item = [SHKItem URL:shareURL title:@"[NSPRME.COM]I find this image inspiring!"];
            item.text = @"I found this picture inspiring and shared it on Nsprme.com! Please take a look and let me know what you think.";
            
            [SHKMail shareItem:item];
        }
        if ( twit == YES )
        {
            NSURL *shareURL = [NSURL URLWithString:imgUrl];
            SHKItem *item = [SHKItem URL:shareURL title:@"Inspiring image"];
            item.text = @"Inspiring pic! #Nsprme.";
            
            [SHKTwitter shareItem:item];
        }
        if ( fb == YES )
        {
            NSURL *shareURL = [NSURL URLWithString:imgUrl];
            SHKItem *item = [SHKItem URL:shareURL title:@"Inspiring image"];
            item.text = @"Inspiring pic! Find more at nsprme.com.";
            
            [SHKFacebook shareItem:item];
        }
         */
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    [op start];
}


@end
