//
//  NsprmeSharekitConfigurator.h
//  clickpic
//
//  Created by Sam Contapay on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "DefaultSHKConfigurator.h"

@interface NsprmeSharekitConfigurator : DefaultSHKConfigurator

- (NSString*)facebookAppId;
-(NSString*)appName;
-(NSString*)appURL;
-(NSString*)twitterConsumerKey;
- (NSString*)twitterSecret;
- (NSString*)twitterCallbackUrl;


@end
