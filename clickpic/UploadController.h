//
//  UploadController.h
//  clickpic
//
//  Created by Sam Contapay on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UploadController.h"

@interface UploadController : QuickDialogController

+(QRootElement*)createUploadForm;

@end
