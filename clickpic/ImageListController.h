//
//  ImageListController.h
//  clickpic
//
//  Created by Sam Contapay on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFPhotoEditorController.h"
#import "ShareDialogController.h"
#import <iAd/iAd.h>

@interface ImageListController : UITableViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, AFPhotoEditorControllerDelegate, ShareDialogDelegate, ADBannerViewDelegate>
{
    UIImage *pickedImage;
    NSString *currentSlug;
    ADBannerView *bannerView;
}

-(void)editPicture;
-(void)sendPicture:(UIImage*)pic;
- (IBAction)pressTest:(id)sender;

@end
