//
//  ImageCell.h
//  clickpic
//
//  Created by Sam Contapay on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCell : UITableViewCell
{
    UIImageView *image;
    UILabel *slug;
}

@property(nonatomic, retain)IBOutlet UIImageView *image;
@property(nonatomic, retain)IBOutlet UILabel *slug;

@end
