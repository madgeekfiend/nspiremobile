//
//  PictureDetailController.m
//  clickpic
//
//  Created by Sam Contapay on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PictureDetailController.h"
#import "ShareDialogController.h"

@implementation PictureDetailController
@synthesize img;
@synthesize file_path = filePath;
@synthesize slug;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
//    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
//    img.image = image;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    img.contentMode = UIViewContentModeScaleAspectFit;
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    img.image = image;
    
    [self.navigationController setToolbarHidden:YES];
}


- (void)viewDidUnload
{
    [self setImg:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Button bar
- (IBAction)shareImage:(id)sender {
    ShareDialogController *dlg = (ShareDialogController*)[QuickDialogController controllerForRoot:[ShareDialogController createLinkShareDialog]];
    dlg.slug = slug;
    [self.navigationController pushViewController:dlg animated:YES];
}
@end
