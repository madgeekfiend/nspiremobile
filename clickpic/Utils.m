//
//  Utils.m
//  clickpic
//
//  Created by Sam Contapay on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"

static Utils *_instance = nil;

@implementation Utils


#pragma mark - Singleton accessor

+(Utils*)sharedInstance 
{
    if ( _instance == nil )
    {
        _instance = [[Utils alloc] init];
    }
    
    return _instance;
}

#pragma mark - Email functions
- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    
    return [emailTest evaluateWithObject:candidate];
}


@end
