//
//  NSpireManager.h
//  clickpic
//
//  Created by Sam Contapay on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSpireManager : NSObject


+(NSpireManager*)sharedApplication;

-(void) setUserDefaultUsername:(NSString*)username withPassword:(NSString*)password withToken:(NSString*)token withNickname:(NSString*)nickname;

-(NSString*)getNickname;
-(NSString*)getToken;
-(NSString*)getUsername;
-(NSString*)getPassword;

-(void)saveImageWithURL:(NSString*)url withSlug:(NSString*)slug withDeleteHash:(NSString*)hash;

-(NSDictionary*)decodeJson:(id)dataObject;
-(NSArray*)getAllImages;
-(BOOL)isUserLoggedIn;
-(NSString*)signParams:(NSMutableDictionary*)dictionary;
-(NSString*)signString:(NSString*)thisstring;
-(void)clearDefaults;

-(void)deleteAtIndex:(NSInteger)slug;

@end
