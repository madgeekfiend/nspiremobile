//
//  Constants.h
//  clickpic
//
//  Created by Sam Contapay on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

extern NSString* const POST_IMAGE_URL;
extern NSString* const BASE_URL;
extern NSString* const SEED_KEY;
extern NSString * const SHORT_IMG_URL;