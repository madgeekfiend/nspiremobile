//
//  NSpireManager.m
//  clickpic
//
//  Created by Sam Contapay on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSpireManager.h"
#import "AppDelegate.h"
#import "JSONKit.h"
#import "Constants.h"
#import "NSString+MD5.h"

static NSpireManager *_instance = nil;

@implementation NSpireManager

#pragma mark - User persistance

-(void) setUserDefaultUsername:(NSString*)username withPassword:(NSString*)password withToken:(NSString*)token withNickname:(NSString *)nickname
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    [defs setObject:username forKey:@"username"];
    [defs setObject:password forKey:@"password"];
    [defs setObject:token forKey:@"token"];
    [defs setObject:nickname forKey:@"nickname"];
    
    [defs synchronize];
}

-(void)clearDefaults
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    [defs removeObjectForKey:@"username"];
    [defs removeObjectForKey:@"password"];
    [defs removeObjectForKey:@"token"];
    [defs removeObjectForKey:@"nickname"];
    
    [defs synchronize];
}

-(BOOL)isUserLoggedIn
{
    if ( [self getToken] != nil )
        return YES;
    
    return NO;
}

-(NSString*)getToken
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    return [defs objectForKey:@"token"];
}

-(NSString*)getUsername
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    return [defs objectForKey:@"username"];
}

-(NSString*)getPassword
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    return [defs objectForKey:@"password"];
}

-(NSString*)getNickname
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    return [defs objectForKey:@"nickname"];
}

#pragma mark - JSON decode
-(NSDictionary *)decodeJson:(id)dataObject
{
    // Get JSON and do stuff
    JSONDecoder *decoder = [[JSONDecoder alloc] initWithParseOptions:JKParseOptionNone];
    return [decoder objectWithData:dataObject];
}

#pragma mark - Core Data

-(void)saveImageWithURL:(NSString *)url withSlug:(NSString *)slug withDeleteHash:(NSString *)hash
{
 
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    NSManagedObject *image = [[NSManagedObject alloc] initWithEntity:[NSEntityDescription entityForName:@"Image" inManagedObjectContext:[app managedObjectContext]] insertIntoManagedObjectContext:[app managedObjectContext]];
    [image setValue:url forKey:@"url"];
    [image setValue:slug forKey:@"slug"];
    [image setValue:hash forKey:@"delete_hash"];
    [image setValue:[NSDate date] forKey:@"created_at"]; 
    
    [[app managedObjectContext] save:nil];
}

-(NSArray*)getAllImages
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Image"];
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    return [[app managedObjectContext] executeFetchRequest:request error:nil];
}

-(void)deleteAtIndex:(NSInteger)slug
{
    NSManagedObject *object = [[self getAllImages] objectAtIndex:slug];
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    [[app managedObjectContext] deleteObject:object];
    
    [[app managedObjectContext] save:nil];
}

-(NSString*)signParams:(NSMutableDictionary *)dictionary
{
    NSArray *myKeys = [dictionary allKeys];
    NSArray *sortedKeys = [myKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    NSMutableArray *parms = [[NSMutableArray alloc] initWithCapacity:20];
    for( NSString* key in sortedKeys )
    {
        [parms addObject:[dictionary objectForKey:key] ];
    }
    // Add the salt key to the end
    [parms addObject:SEED_KEY];
    NSString *sigthis = [parms componentsJoinedByString:@""];
    NSLog(@"sign this: %@", sigthis);
    // Make an MD5 sig
    return [sigthis MD5]; 
}

-(NSString *)signString:(NSString *)thisstring
{
    return [thisstring MD5];
}

#pragma mark - Instance creation

+(NSpireManager*)sharedApplication
{
    if ( _instance == nil )
        _instance = [[NSpireManager alloc] init];
    
    return _instance;
}

@end
