//
//  UploadController.m
//  clickpic
//
//  Created by Sam Contapay on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UploadController.h"

@implementation UploadController



+(QRootElement*)createUploadForm
{
    QRootElement *root = [[QRootElement alloc] init];
    root.grouped = YES;
    root.controllerName = @"UploadController";
    
    QSection *pic = [[QSection alloc] initWithTitle:@"Add Picture"];
    QButtonElement *addPic = [[QButtonElement alloc] initWithTitle:@"Add Picture"];
    [pic addElement: addPic];
    
    QSection *main = [[QSection alloc] init];
    QEntryElement *caption = [[QEntryElement alloc] initWithTitle:@"Caption" Value:@"" Placeholder:@"Enter Caption"];
    [main addElement:caption];
    
    [root addSection:pic];
    [root addSection: main];
    
    return root;
}


@end
